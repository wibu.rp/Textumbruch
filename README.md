# Function Kata "Textumbruch"

siehe [ccd-school.de](http://ccd-school.de/coding-dojo/function-katas/textumbruch)

![Alt text](https://g.gravizo.com/svg?
digraph Textumbruch {
   rankdir=LR;
   in [style = invis];
   out [style = invis];
   Umbrechen [shape = circle]
   in -> Umbrechen [label="Text, max Zeilenlaenge"];
   Umbrechen -> out [label = "Text'"];
  }
)
