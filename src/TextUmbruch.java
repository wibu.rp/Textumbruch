import java.util.ArrayList;
import java.util.List;


public class TextUmbruch {

	public class Zeile {
		private List<String> _woerter;
		private int _laenge;
		private int _maxlaenge;
		
		public int laenge() { return _laenge; }
		public List<String> woerter() { return _woerter; }
		
		public Zeile(String wort, int maxlaenge) {
			_woerter = new ArrayList<String>();
			_laenge = 0;
			_maxlaenge = maxlaenge;
			wortAnhaengen(wort);
		}
		
		public void wortAnhaengen(String wort) {
			_woerter.add(wort);
			if (_laenge == 0) {
				_laenge = wort.length();
			} else {
				_laenge += " ".length() + wort.length();
			}
		}
		
		public boolean passtDasWort(String wort) {
			return _laenge + " ".length() + wort.length() <= _maxlaenge;
		}
		
		public String formatieren() {
			String zeile = _woerter.get(0);
			for (int i = 1; i < _woerter.size(); i++) {
				zeile += " " + _woerter.get(i);
			}
			return zeile;
		}
	}
	
    public String umbrechen(String text, int maximaleZeilenlaenge) {
        String[] wortListe = wortlisteErstellen(text);
        wortListe = langeWoerterTrennen(wortListe, maximaleZeilenlaenge);
        return  textFormatieren(wortListe, maximaleZeilenlaenge);
    }

    private String[] wortlisteErstellen(String text) {
        return text.replaceAll(System.lineSeparator(), " ").split(" ");
    }

    private String[] langeWoerterTrennen(String[] wortliste, int maximaleZeilenlaenge) {
        List<String> neueWortliste = new ArrayList<>();

        for (int i = 0; i < wortliste.length; i++) {
            String neuesWort = wortliste[i];
            while (neuesWort.length() > maximaleZeilenlaenge) {
                neueWortliste.add(neuesWort.substring(0, maximaleZeilenlaenge));
                neuesWort = neuesWort.substring(maximaleZeilenlaenge);
            }
            neueWortliste.add(neuesWort);
        }
        return neueWortliste.toArray(new String[neueWortliste.size()]);
    }

    private String textFormatieren(String[] wortliste, int maximaleZeilenlaenge) {
        String formatierterText = wortliste[0];
        int zeilenlaenge = wortliste[0].length();

        for (int i = 1; i < wortliste.length; i++) {
            if (zeilenlaenge + 1 + wortliste[i].length() > maximaleZeilenlaenge) {
                formatierterText += System.lineSeparator() + wortliste[i];
                zeilenlaenge = wortliste[i].length();
            } else {
                formatierterText += " " + wortliste[i];
                zeilenlaenge += 1 + wortliste[i].length();
            }
        }
        return formatierterText;
    }
    
    
    private List<Zeile> zeilenZusammensetzen(String[] wortliste, int maximaleZeilenlaenge) {
    	List<Zeile> zeilenListe = new ArrayList<>();
    	Zeile zeile = new Zeile(wortliste[0], maximaleZeilenlaenge);
    	
        for (int i = 1; i < wortliste.length; i++) {
            if (zeile.passtDasWort(wortliste[i])) {
            	zeile.wortAnhaengen(wortliste[i]);
            } else {
            	zeilenListe.add(zeile);
            	zeile = new Zeile(wortliste[i], maximaleZeilenlaenge);
            }
        }
        return zeilenListe;
    }
    
    private String zeilenFormatieren(List<Zeile> zeilenListe) {
    	String text = "";
    	
    	for (Zeile zeile : zeilenListe) {
    		text += zeile.formatieren() + System.lineSeparator();
    	}
    	return text;
    }

}
