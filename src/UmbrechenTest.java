import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UmbrechenTest {

    @Test
    public void testUmbrechen() {
        String inputString = "Es blaut die Nacht," + System.lineSeparator()
                + "die Sternlein blinken," + System.lineSeparator()
                + "Schneeflöcklein leis hernieder sinken.";

        String expectedOutputString =
                "Es blaut" + System.lineSeparator()
                +"die"+ System.lineSeparator()
                +"Nacht,"+ System.lineSeparator()
                +"die"+ System.lineSeparator()
                +"Sternlein"+ System.lineSeparator()
                +"blinken,"+ System.lineSeparator()
                +"Schneeflö"+ System.lineSeparator()
                +"cklein"+ System.lineSeparator()
                +"leis"+ System.lineSeparator()
                +"hernieder"+ System.lineSeparator()
                +"sinken.";
        assertEquals(expectedOutputString, new TextUmbruch().umbrechen(inputString, 9));
    }
}